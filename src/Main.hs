module Main where

-- Standard library imports
import Data.Sequence (iterateN, dropWhileL, index)
import System.Environment (getArgs)


-- Type definitions describing a sudoku board
type Cell = [Int]
type Row = [Cell]
type Board = [Row]


-- Functions to read in boards from strings
readCell :: String -> Cell
readCell "-" = [1..9]
readCell c = [read c]

readRow :: String -> Row
readRow row = map readCell $ words row

readBoard :: String -> Board
readBoard board = map readRow $ lines board


-- Functions to write boards in a user-friendly format
writeCell :: Cell -> String
writeCell [n] = show n
writeCell _ = "-"

writeRow :: Row -> String
writeRow row = foldl1 (\x y -> x ++ " " ++ y) $ map writeCell row

writeBoard :: Board -> String
writeBoard board = foldl1 (\x y -> x ++ "\n" ++ y) $ map writeRow board

printBoard :: Board -> IO ()
printBoard = putStrLn . writeBoard


-- Functions to mutate boards. Note: each transform is its own inverse.
txfrmRows :: Board -> Board
txfrmRows board = board

txfrmCols :: Board -> Board
txfrmCols board = [map (!!n) board | n <- [0..8]]

txfrmSqrs :: Board -> Board
txfrmSqrs board = [[(board !! (3 * m + i)) !! (3 * n + j)
                   | i <- [0..2], j <- [0..2]]
                   | m <- [0..2], n <- [0..2]]


-- Functions to solve the Sudoku board
count :: (Eq a) => a -> [a] -> Int
count x xs = length $ filter (==x) xs

solvedCell :: Cell -> Bool
solvedCell cell = (length cell) == 1

solvedRow :: Row -> Bool
solvedRow row = all solvedCell row

solvedBoard :: Board -> Bool
solvedBoard board = all solvedRow board

-- This kernel finds solved cells (cells with only one possible solution)
-- and removes the value of that cell from any other cells in the same row.
solvedValues :: Row -> [Int]
solvedValues row = concat $ filter solvedCell row

removeSolvedValues :: Row -> Row
removeSolvedValues row = foldl remove row $ solvedValues row
    where   remove r n = map (delete n) r
            delete n [i] = [i]
            delete n cell = filter (/= n) cell

-- This kernell finds cells which are the only one in the row to contain
-- a particular value. In this case we know that cell must have that value
-- and can remove all other values making the cell 'solved'.
singularValues :: Row -> [Int]
singularValues row = filter singular [1..9]
    where   singular n = (count n $ concat row) == 1

removeSingularValues :: Row -> Row
removeSingularValues row = foldl remove row $ singularValues row
    where   remove r n = map (delete n) r
            delete :: Int -> Cell -> Cell
            delete n cell   | ((count n cell) > 0)  = [n]
                            | otherwise             = cell


-- Run each kernel over a particular tranform of the board
solveKernel :: Board -> Board
solveKernel board = foldl (\b k -> map k b) board kernels
    where   kernels = [removeSolvedValues, removeSingularValues]

-- Run solveKernel for each of the three board transforms
solveStep :: Board -> Board
solveStep board = foldl (\b t -> t $ solveKernel $ t b) board transforms
    where   transforms = [txfrmRows, txfrmCols, txfrmSqrs]

-- Run solveStep until the board is solved. Note that if our kernels
-- are insufficient to solve the board this function will give up 
-- after 100 iterations.
solve :: Board -> Board
solve board = (\xs -> index xs 0) $ dropWhileL (not . solvedBoard) $ iterateN 100 solveStep board


main :: IO ()
main = do
    -- Read board starting state
    [filename] <- getArgs
    boardStr <- readFile filename
    let board = readBoard boardStr

    -- Print starting state
    putStrLn "Input Board"
    printBoard board

    -- solve the board and print out the result
    putStrLn "\nSolved Board"
    printBoard $ solve board
