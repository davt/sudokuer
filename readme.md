# Sudokuer

A _very_ simple sudoku solver. Written in Haskell as a learning exercise.

## Usage

Build using cabal:
```
$ cabal build
Resolving dependencies...
Configuring sudokuer-0.1.0.0...
Building sudokuer-0.1.0.0...
Preprocessing executable 'sudokuer' for sudokuer-0.1.0.0...
[1 of 1] Compiling Main             ( src/Main.hs, dist/build/sudokuer/sudokuer-tmp/Main.o )
Linking dist/build/sudokuer/sudokuer ...
```

Then run  with the input file as an argument e.g.
```
$ dist/build/sudokuer/sudokuer src/test-board 
Input Board
- - - 9 - - - - -
- - 1 - - 4 - - -
- 4 - - 1 2 - - 7
5 - - - - - 1 - 9
- - 6 - - 3 - 8 -
- 2 4 - 6 - 3 - -
- - - 4 - 6 - 9 8
- - - - 9 - 6 - -
- - 8 7 - - 4 - -

Solved Board
3 6 2 9 7 5 8 4 1
7 5 1 3 8 4 9 2 6
8 4 9 6 1 2 5 3 7
5 8 3 2 4 7 1 6 9
9 7 6 1 5 3 2 8 4
1 2 4 8 6 9 3 7 5
2 1 5 4 3 6 7 9 8
4 3 7 5 9 8 6 1 2
6 9 8 7 2 1 4 5 3
``` 
